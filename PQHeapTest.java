
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.fail;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;

import org.junit.Before;
import org.junit.Test;

public class PQHeapTest {
    PriorityQueue<Integer> queue;
    PriorityQueue<String> queue2;

    @Before
    public void setUp() throws Exception {
        Comparator<String> c = new Comparator<String>() {
            public int compare(String object1, String object2) {
                return object2.compareTo(object1);
            }
        };
        Comparator<Integer> c2 = new Comparator<Integer>() {
            public int compare(Integer object1, Integer object2) {
                return object2.compareTo(object1);
            }
        };
        this.queue = new PQHeap<Integer>(c2);
        this.queue2 = new PQHeap<String>(c);
    }

    @Test
    public void testPQCreation() {
        assertEquals("Queue size is not 0 at creation", 0, this.queue.size());
    }

    @Test
    public void testIsEmptyOnEmptyPQ() {
        assertTrue("Queue is not empty on an empty PQ", this.queue.isEmpty());
    }

    @Test
    public void testClearOnEmptyPQ() {
        this.queue.clear();
        assertEquals("Queue size is not 0 on clearing an empty PQ", 0,
            this.queue.size());
    }

    @Test
    public void testSizeOnEmpty() {
        assertEquals("Size is not 0 on empty PQ", 0, this.queue.size());
    }

    @Test
    public void testPQInitCollection() {
        Collection<Integer> collection = new ArrayList<Integer>();
        collection.add(new Integer(1));
        this.queue.init(collection);

        assertEquals("Init with collection does not work", 1,
            this.queue.size());
    }

    @Test
    public void testPQInitCollectionString() {
        Collection<String> collection = new ArrayList<String>();
        collection.add("a");
        this.queue2.init(collection);

        assertEquals("Init with collection does not work", 1,
            this.queue2.size());
    }

    @Test
    public void testPQInitCollectionOverwrite() {
        this.queue.insert(1);
        this.queue.insert(2);
        Collection<Integer> collection = new ArrayList<Integer>();
        collection.add(new Integer(3));
        collection.add(new Integer(4));
        collection.add(new Integer(5));
        this.queue.init(collection);

        assertEquals("Init does not overwrite old PQ properly", 3,
            this.queue.size());
        assertEquals("Init overwrite does not establish max value properly",
            new Integer(5), this.queue.peek());
    }

    @Test
    public void testPQInitCollectionOverwriteString() {
        this.queue2.insert("a");
        this.queue2.insert("b");
        Collection<String> collection = new ArrayList<String>();
        collection.add("c");
        collection.add("d");
        collection.add("e");
        this.queue2.init(collection);

        assertEquals("Init does not overwrite old PQ properly", 3,
            this.queue2.size());
        assertEquals("Init overwrite does not establish max value properly",
            "e", this.queue2.peek());
    }

    @Test
    public void testpeekOnEmptyQueue() {
        try {
            this.queue.peek();
            fail("peek on an empty queue did not throw a QueueEmptyException");
        } catch (QueueEmptyException e) {

        }
    }

    @Test
    public void testremoveOnEmptyQueue() {
        try {
            this.queue.remove();
            fail("remove on an empty queue did not throw a "
                + "QueueEmptyException");
        } catch (QueueEmptyException e) {

        }
    }

    @Test
    public void testSizeOnSingleItem() {
        this.queue.insert(new Integer(1));

        assertEquals("Size is not 1 after one insert", 1, this.queue.size());
    }

    @Test
    public void testSizeOnSingleItemString() {
        this.queue2.insert("a");

        assertEquals("Size is not 1 after one insert", 1, this.queue2.size());
    }

    @Test
    public void testClearOnSingleItem() {
        this.queue.insert(new Integer(1));
        this.queue.clear();

        assertEquals("Queue size is not 0 on clearing a single item", 0,
            this.queue.size());
    }

    @Test
    public void testClearOnSingleItemString() {
        this.queue2.insert("a");
        this.queue2.clear();

        assertEquals("Queue size is not 0 on clearing a single item", 0,
            this.queue2.size());
    }

    @Test
    public void testInsertOnSingleItem() {
        this.queue.insert(new Integer(1));

        assertEquals("Queue size did not increase after insert", 1,
            this.queue.size());
    }

    @Test
    public void testInsertOnSingleItemString() {
        this.queue2.insert("a");

        assertEquals("Queue size did not increase after insert", 1,
            this.queue2.size());
    }

    @Test
    public void testIsEmptyOnSingleItem() {
        this.queue.insert(new Integer(1));

        assertFalse("Queue is empty after inserting an element",
            this.queue.isEmpty());
    }

    @Test
    public void testIsEmptyOnSingleItemString() {
        this.queue2.insert("a");

        assertFalse("Queue is empty after inserting an element",
            this.queue2.isEmpty());
    }

    @Test
    public void testpeekOnSingleItem() {
        this.queue.insert(new Integer(1));

        assertEquals("Get Max did not retrieve the only item in PQ",
            new Integer(1), this.queue.peek());
    }

    @Test
    public void testpeekOnSingleItemString() {
        this.queue2.insert("a");

        assertEquals("Get Max did not retrieve the only item in PQ", "a",
            this.queue2.peek());
    }

    @Test
    public void testremoveOnSingleItem() {
        this.queue.insert(new Integer(1));

        assertEquals("Remove Max did not retrieve the only item in PQ",
            new Integer(1), this.queue.remove());
    }

    @Test
    public void testremoveOnSingleItemString() {
        this.queue2.insert("a");

        assertEquals("Remove Max did not retrieve the only item in PQ",
            "a", this.queue2.remove());
    }

    @Test
    public void testremoveWillProducePQSizeZero() {
        this.queue.insert(new Integer(1));

        assertEquals("Remove Max did not retrieve the only in PQ",
            new Integer(1), this.queue.remove());
        assertEquals("Remove Max did not remove the item from the PQ",
            0, this.queue.size());
    }

    @Test
    public void testremoveWillProducePQSizeZeroString() {
        this.queue2.insert("a");

        assertEquals("Remove Max did not retrieve the only in PQ", "a",
            this.queue2.remove());
        assertEquals("Remove Max did not remove the item from the PQ", 0,
            this.queue2.size());
    }

    @Test
    public void testremoveWillProducePQEmpty() {
        this.queue.insert(new Integer(1));

        assertEquals("Remove Max did not retrieve the only in PQ",
            new Integer(1), this.queue.remove());
        assertTrue("Remove Max did not remove the item from the PQ",
            this.queue.isEmpty());
    }

    @Test
    public void testremoveWillProducePQEmptyString() {
        this.queue2.insert("a");

        assertEquals("Remove Max did not retrieve the only in PQ", "a",
            this.queue2.remove());
        assertTrue("Remove Max did not remove the item from the PQ",
            this.queue2.isEmpty());
    }

    @Test
    public void testSizeOnMultipleItems() {
        this.queue.insert(new Integer(1));
        this.queue.insert(new Integer(2));
        this.queue.insert(new Integer(3));
        this.queue.insert(new Integer(4));
        this.queue.insert(new Integer(5));

        assertEquals("Size is not 5 after 5 inserts", 5, this.queue.size());
    }

    @Test
    public void testSizeOnMultipleItemsString() {
        this.queue2.insert("aa");
        this.queue2.insert("ba");
        this.queue2.insert("ca");
        this.queue2.insert("da");
        this.queue2.insert("ea");

        assertEquals("Size is not 5 after 5 inserts", 5, this.queue2.size());
    }

    @Test
    public void testSizeOnMultipleItemsWithRemove() {
        this.queue.insert(new Integer(1));
        this.queue.insert(new Integer(2));
        this.queue.insert(new Integer(3));
        assertEquals("Size is not 3 after 3 inserts", 3, this.queue.size());
        this.queue.remove();
        assertEquals("Size is not 2 after 3 inserts and 1 delete", 2,
            this.queue.size());
        this.queue.insert(new Integer(7));
        assertEquals("Size is not 3 after 4 inserts and 1 delete", 3,
            this.queue.size());
    }

    @Test
    public void testSizeOnMultipleItemsWithRemoveString() {
        this.queue2.insert("aa");
        this.queue2.insert("ba");
        this.queue2.insert("ca");
        assertEquals("Size is not 3 after 3 inserts", 3, this.queue2.size());
        this.queue2.remove();
        assertEquals("Size is not 2 after 3 inserts and 1 delete", 2,
            this.queue2.size());
        this.queue2.insert("da");
        assertEquals("Size is not 3 after 4 inserts and 1 delete", 3,
            this.queue2.size());

    }

    @Test
    public void testClearOnMultipleItems() {
        this.queue.insert(new Integer(1));
        this.queue.insert(new Integer(2));
        this.queue.insert(new Integer(3));
        this.queue.insert(new Integer(4));
        this.queue.insert(new Integer(5));

        this.queue.clear();

        assertEquals("Queue size is not 0 at creation", 0, this.queue.size());
        assertTrue("Queue is not empty at creation", this.queue.isEmpty());
    }

    @Test
    public void testClearOnMultipleItemsString() {
        this.queue2.insert("aa");
        this.queue2.insert("ba");
        this.queue2.insert("ca");
        this.queue2.insert("da");
        this.queue2.insert("ea");

        this.queue2.clear();

        assertEquals("Queue size is not 0 at creation", 0, this.queue2.size());
        assertTrue("Queue is not empty at creation", this.queue2.isEmpty());
    }

    @Test
    public void testIsEmptyOnMutlipleItems() {
        this.queue.insert(new Integer(1));
        this.queue.insert(new Integer(2));
        this.queue.insert(new Integer(3));
        this.queue.insert(new Integer(4));
        this.queue.insert(new Integer(5));

        assertFalse("Queue is empty after inserting multiple items",
            this.queue.isEmpty());
    }

    @Test
    public void testIsEmptyOnMutlipleItemsString() {
        this.queue2.insert("aa");
        this.queue2.insert("ba");
        this.queue2.insert("ca");
        this.queue2.insert("da");
        this.queue2.insert("ea");

        assertFalse("Queue is empty after inserting multiple items",
            this.queue2.isEmpty());
    }

    @Test
    public void testInsertOnMultipleItemsInOrder() {
        this.queue.insert(new Integer(1));
        this.queue.insert(new Integer(2));
        this.queue.insert(new Integer(3));
        this.queue.insert(new Integer(4));
        this.queue.insert(new Integer(5));

        assertEquals("Queue size does not reflect the number of inserts",
            5, this.queue.size());
    }

    @Test
    public void testInsertOnMultipleItemsInOrderString() {
        this.queue2.insert("aa");
        this.queue2.insert("ba");
        this.queue2.insert("ca");
        this.queue2.insert("da");
        this.queue2.insert("ea");

        assertEquals("Queue size does not reflect the number of inserts",
            5, this.queue2.size());
    }

    @Test
    public void testpeekOnMultipleItemsInOrder() {
        this.queue.insert(new Integer(1));
        this.queue.insert(new Integer(2));
        this.queue.insert(new Integer(3));
        this.queue.insert(new Integer(4));
        this.queue.insert(new Integer(5));

        assertEquals("Get Max did not retrieve the highest value when in order",
            new Integer(5), this.queue.peek());
    }

    @Test
    public void testpeekOnMultipleItemsInOrderString() {
        this.queue2.insert("aa");
        this.queue2.insert("ba");
        this.queue2.insert("ca");
        this.queue2.insert("da");
        this.queue2.insert("ea");

        assertEquals("Get Max did not retrieve the highest value when in order",
            "ea", this.queue2.peek());
    }

    @Test
    public void testremoveOnMultipleItemsInOrder() {
        this.queue.insert(new Integer(1));
        this.queue.insert(new Integer(2));
        this.queue.insert(new Integer(3));
        this.queue.insert(new Integer(4));
        this.queue.insert(new Integer(5));

        assertEquals("Remove Max did not retrieve the item in PQ when in order",
            new Integer(5), this.queue.remove());
        assertEquals("Remove Max did not remove the item from PQ", 4,
            this.queue.size());
    }

    @Test
    public void testremoveOnMultipleItemsInOrderString() {
        this.queue2.insert("aa");
        this.queue2.insert("ba");
        this.queue2.insert("ca");
        this.queue2.insert("da");
        this.queue2.insert("ea");

        assertEquals("Remove Max did not retrieve the item in PQ when in order",
            "ea", this.queue2.remove());
        assertEquals("Remove Max did not remove the item from PQ", 4,
            this.queue2.size());
    }

    @Test
    public void testpeekOnMultipleItemsReverseOrder() {
        this.queue.insert(new Integer(5));
        this.queue.insert(new Integer(4));
        this.queue.insert(new Integer(3));
        this.queue.insert(new Integer(2));
        this.queue.insert(new Integer(1));

        assertEquals("Get Max did not retrieve the highest value when in "
            + "reverse order", new Integer(5), this.queue.peek());
    }

    @Test
    public void testpeekOnMultipleItemsReverseOrderString() {
        this.queue2.insert("ea");
        this.queue2.insert("da");
        this.queue2.insert("ca");
        this.queue2.insert("ba");
        this.queue2.insert("aa");

        assertEquals("Get Max did not retrieve the highest value when in "
            + "reverse order", "ea", this.queue2.peek());
    }

    @Test
    public void testremoveOnMultipleItemsReverseOrder() {
        this.queue.insert(new Integer(5));
        this.queue.insert(new Integer(4));
        this.queue.insert(new Integer(3));
        this.queue.insert(new Integer(2));
        this.queue.insert(new Integer(1));

        assertEquals("Remove Max did not retrieve the item in PQ when in "
            + "order", new Integer(5), this.queue.remove());
        assertEquals("Remove Max did not remove the item from PQ", 4,
            this.queue.size());
    }

    @Test
    public void testremoveOnMultipleItemsReverseOrderString() {
        this.queue2.insert("ea");
        this.queue2.insert("da");
        this.queue2.insert("ca");
        this.queue2.insert("ba");
        this.queue2.insert("aa");

        assertEquals("Remove Max did not retrieve the item in PQ when "
            + "in order", "ea", this.queue2.remove());
        assertEquals("Remove Max did not remove the item from PQ", 4,
            this.queue2.size());
    }

    @Test
    public void testpeekOnMultipleItemsRandomOrder() {
        this.queue.insert(new Integer(3));
        this.queue.insert(new Integer(5));
        this.queue.insert(new Integer(2));
        this.queue.insert(new Integer(4));
        this.queue.insert(new Integer(1));

        assertEquals("Get Max did not retrieve the highest value when in "
            + "random order", new Integer(5), this.queue.peek());
    }

    @Test
    public void testpeekOnMultipleItemsRandomOrderString() {
        this.queue2.insert("ca");
        this.queue2.insert("ea");
        this.queue2.insert("ba");
        this.queue2.insert("da");
        this.queue2.insert("aa");

        assertEquals("Get Max did not retrieve the highest value when in "
            + "random order", "ea", this.queue2.peek());
    }

    @Test
    public void testremoveOnMultipleItemsRandomOrder() {
        this.queue.insert(new Integer(3));
        this.queue.insert(new Integer(5));
        this.queue.insert(new Integer(2));
        this.queue.insert(new Integer(4));
        this.queue.insert(new Integer(1));

        assertEquals("Remove Max did not retrieve the item in PQ when in "
            + "order", new Integer(5), this.queue.remove());
        assertEquals("Remove Max did not remove the item from PQ", 4,
            this.queue.size());
    }

    @Test
    public void testremoveOnMultipleItemsRandomOrderString() {
        this.queue2.insert("ca");
        this.queue2.insert("ea");
        this.queue2.insert("ba");
        this.queue2.insert("da");
        this.queue2.insert("aa");

        assertEquals("Remove Max did not retrieve the item in PQ when "
            + "in order", "ea", this.queue2.remove());
        assertEquals("Remove Max did not remove the item from PQ", 4,
            this.queue2.size());
    }

    @Test
    public void testInsertingDuplicates() {
        this.queue.insert(new Integer(2));
        this.queue.insert(new Integer(2));

        assertEquals("Duplicates were not formed with two inserts of same "
            + "value", 2, this.queue.size());
    }

    @Test
    public void testInsertingDuplicatesString() {
        this.queue2.insert("ba");
        this.queue2.insert("ba");

        assertEquals("Duplicates were not formed with two inserts of same"
            + "value", 2, this.queue2.size());
    }

    @Test
    public void testpeekOnDuplicates() {
        this.queue.insert(new Integer(2));
        this.queue.insert(new Integer(2));

        assertEquals("Get Max stopped working after inserting duplicates",
            new Integer(2), this.queue.peek());
    }

    @Test
    public void testpeekOnDuplicatesString() {
        this.queue2.insert("ba");
        this.queue2.insert("ba");

        assertEquals("Get Max stopped working after inserting duplicates",
            "ba", this.queue2.peek());
    }

    @Test
    public void testremoveOnDuplicates() {
        this.queue.insert(new Integer(2));
        this.queue.insert(new Integer(2));

        assertEquals("Remove Max did not return the max element after "
            + "inserting duplicates", new Integer(2), this.queue.remove());
        assertEquals("Remove Max did not return the duplicate",
            new Integer(2), this.queue.remove());
        assertTrue("Removes did not empty the queue of duplicates",
            this.queue.isEmpty());
    }

    @Test
    public void testremoveOnDuplicatesString() {
        this.queue2.insert("ba");
        this.queue2.insert("ba");

        assertEquals("Remove Max did not return the max element after "
            + "inserting duplicates", "ba", this.queue2.remove());
        assertEquals("Remove Max did not return the duplicate", "ba",
            this.queue2.remove());
        assertTrue("Removes did not empty the queue of duplicates",
            this.queue2.isEmpty());
    }

    @Test
    public void testremoveOnEachElement() {
        this.queue.insert(1);
        this.queue.insert(5);
        this.queue.insert(3);
        this.queue.insert(4);
        this.queue.insert(2);

        assertEquals("Remove Max did not return the highest element",
            new Integer(5), this.queue.remove());
        assertEquals("Size did not increment properly on removal", 4,
            this.queue.size());
        assertEquals("Remove Max did not return the second highest element",
            new Integer(4), this.queue.remove());
        assertEquals("Size did not increment properly on removal", 3,
            this.queue.size());
        assertEquals("Remove Max did not return the third highest element",
            new Integer(3), this.queue.remove());
        assertEquals("Size did not increment properly on removal", 2,
            this.queue.size());
        assertEquals("Remove Max did not return the fourth highest element",
            new Integer(2), this.queue.remove());
        assertEquals("Size did not increment properly on removal", 1,
            this.queue.size());
        assertEquals("Remove Max did not return the fifth highest element",
            new Integer(1), this.queue.remove());
        assertEquals("Size did not increment properly on removal", 0,
            this.queue.size());
        assertTrue("Removal of multiple elements did not empty the queue",
            this.queue.isEmpty());
    }

    @Test
    public void testremoveOnEachElementString() {
        this.queue2.insert("ca");
        this.queue2.insert("ea");
        this.queue2.insert("ba");
        this.queue2.insert("da");
        this.queue2.insert("aa");

        assertEquals("Remove Max did not return the highest element",
            "ea", this.queue2.remove());
        assertEquals("Size did not increment properly on removal", 4,
            this.queue2.size());
        assertEquals("Remove Max did not return the second highest element",
            "da", this.queue2.remove());
        assertEquals("Size did not increment properly on removal", 3,
            this.queue2.size());
        assertEquals("Remove Max did not return the third highest element",
            "ca", this.queue2.remove());
        assertEquals("Size did not increment properly on removal", 2,
            this.queue2.size());
        assertEquals("Remove Max did not return the fourth highest element",
            "ba", this.queue2.remove());
        assertEquals("Size did not increment properly on removal", 1,
            this.queue2.size());
        assertEquals("Remove Max did not return the fifth highest element",
            "aa", this.queue2.remove());
        assertEquals("Size did not increment properly on removal", 0,
            this.queue2.size());
        assertTrue("Removal of multiple elements did not empty the queue",
            this.queue2.isEmpty());
    }

    @Test
    public void testremoveOnEachElementWithDuplicates() {
        this.queue.insert(12); // second highest element
        this.queue.insert(1);
        this.queue.insert(5);
        this.queue.insert(-10); // tenth highest element (lowest)
        this.queue.insert(3);
        this.queue.insert(4);
        this.queue.insert(2);
        this.queue.insert(15); // highest element
        this.queue.insert(1);
        this.queue.insert(5);
        this.queue.insert(3);
        this.queue.insert(4);
        this.queue.insert(2);
        this.queue.insert(10); // third highest element
        this.queue.insert(-5); // ninth highest element

        assertEquals("Remove Max did not return the highest element",
            new Integer(15), this.queue.remove());
        assertEquals("Remove Max did not return the second highest element",
            new Integer(12), this.queue.remove());
        assertEquals("Remove Max did not return the third highest element",
            new Integer(10), this.queue.remove());
        assertEquals("Remove Max did not return the fourth highest element",
            new Integer(5), this.queue.remove());
        assertEquals("Remove Max did not return the fourth highest element "
            + "duplicate", new Integer(5), this.queue.remove());
        assertEquals("Remove Max did not return the fifth highest element",
            new Integer(4), this.queue.remove());
        assertEquals("Remove Max did not return the fifth highest element "
            + "duplicate", new Integer(4), this.queue.remove());
        assertEquals("Remove Max did not return the sixth highest element",
            new Integer(3), this.queue.remove());
        assertEquals("Remove Max did not return the sixth highest element"
            + " duplicate", new Integer(3), this.queue.remove());
        assertEquals("Remove Max did not return the seventh highest element",
            new Integer(2), this.queue.remove());
        assertEquals("Remove Max did not return the seventh highest element"
            + " duplicate", new Integer(2), this.queue.remove());
        assertEquals("Remove Max did not return the eigth highest element",
            new Integer(1), this.queue.remove());
        assertEquals("Remove Max did not return the eigth highest element"
            + " duplicate", new Integer(1), this.queue.remove());
        assertEquals("Remove Max did not return the ninth highest element",
            new Integer(-5), this.queue.remove());
        assertEquals("Remove Max did not return the tenth highest element",
            new Integer(-10), this.queue.remove());
        assertTrue("Queue not empty after all removals of duplicates",
            this.queue.isEmpty());
    }

    @Test
    public void testremoveOnEachElementWithDuplicatesString() {
        this.queue2.insert("ea"); //Fourth Highest Element
        this.queue2.insert("ea"); //Fourth Duplicate
        this.queue2.insert("da"); //Fifth Highest Element
        this.queue2.insert("da"); //Fifth Duplicate
        this.queue2.insert("ca"); //Sixth Highest Element
        this.queue2.insert("ca"); //Sixth Duplicate
        this.queue2.insert("ba"); //Seventh Highest Element
        this.queue2.insert("ba"); //Seventh Duplicate
        this.queue2.insert("aa"); //Eighth Highest Element
        this.queue2.insert("aa"); //Eighth Duplicate
        this.queue2.insert("ha"); //Highest Element
        this.queue2.insert("ga"); //Second Highest Element
        this.queue2.insert("fa"); //Third Highest Element

        assertEquals("Remove Max did not return the highest element", "ha",
            this.queue2.remove());
        assertEquals("Remove Max did not return the second highest element",
            "ga", this.queue2.remove());
        assertEquals("Remove Max did not return the third highest element",
            "fa", this.queue2.remove());
        assertEquals("Remove Max did not return the fourth highest element",
            "ea", this.queue2.remove());
        assertEquals("Remove Max did not return the fourth highest element"
            + "duplicate", "ea", this.queue2.remove());
        assertEquals("Remove Max did not return the fifth highest element",
            "da", this.queue2.remove());
        assertEquals("Remove Max did not return the fifth highest element "
            + " duplicate", "da", this.queue2.remove());
        assertEquals("Remove Max did not return the sixth highest element",
            "ca", this.queue2.remove());
        assertEquals("Remove Max did not return the sixth highest element "
            + "duplicate", "ca", this.queue2.remove());
        assertEquals("Remove Max did not return the seventh highest element",
            "ba", this.queue2.remove());
        assertEquals("Remove Max did not return the seventh highest element"
            + " duplicate", "ba", this.queue2.remove());
        assertEquals("Remove Max did not return the eigth highest element",
            "aa", this.queue2.remove());
        assertEquals("Remove Max did not return the eigth highest element"
            + " duplicate", "aa", this.queue2.remove());
        assertTrue("Queue not empty after all removals of duplicates",
            this.queue2.isEmpty());
    }
}
