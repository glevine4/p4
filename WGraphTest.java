

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

/** Set of Junit tests for our Graph implementations.
 */
public class WGraphTest {
    WGraphP4<String> g;
    GVertex<String> a, b, v, u, x, y;
    WEdge<String> edgeVU, edgeVX, edgeAX, edgeAB, edgeBV, edgeAY;

    @Before
    public void setupGraph() {
        this.g = new WGraphP4<String>();
        this.a = new GVertex<String>("a", this.g.nextID());
        this.b = new GVertex<String>("b", this.g.nextID());
        this.v = new GVertex<String>("v", this.g.nextID());
        this.u = new GVertex<String>("u", this.g.nextID());
        this.x = new GVertex<String>("x", this.g.nextID());
        this.y = new GVertex<String>("y", this.g.nextID());
        this.edgeVU = new WEdge<String>(this.v, this.u, 1);
        this.edgeVX = new WEdge<String>(this.v, this.x, 2);
        this.edgeAX = new WEdge<String>(this.a, this.x, 4);
        this.edgeAB = new WEdge<String>(this.a, this.b, 7);
        this.edgeBV = new WEdge<String>(this.b, this.v, 3);
    }

    public void addSetUp() {
        this.g.addVertex(this.a);
        this.g.addVertex(this.b);
        this.g.addVertex(this.v);
        this.g.addVertex(this.u);
        this.g.addVertex(this.x);
        this.g.addVertex(this.y);
        this.g.addEdge(this.edgeVU);
        this.g.addEdge(this.edgeVX);
        this.g.addEdge(this.edgeAX);
        this.g.addEdge(this.edgeAB);
        this.g.addEdge(this.edgeBV);
    }

    @Test
    public void testEmpty() {
        assertEquals(0, this.g.numEdges());
        assertEquals(0, this.g.numVerts());
        this.addSetUp();
        assertEquals(6, this.g.numVerts());
        assertEquals(5, this.g.numEdges());
        this.g.clear();
    }

    @Test
    public void testAddVertex() {
        assertEquals(true, this.g.addVertex(this.v));
        assertEquals(true, this.g.addVertex(this.u));
        assertEquals(false, this.g.addVertex(this.v));
        assertEquals(2, this.g.numVerts());
        assertEquals(0, this.g.numEdges());
        this.g.clear();
    }
    public void testNextID() {
        this.addSetUp();
        assertEquals(0, this.a.id());
        assertEquals(6, this.g.nextID());
        this.g.clear();
    }

    @Test
    public void testAddEdge() {
        assertEquals(true, this.g.addEdge(this.edgeVU));
        assertEquals(true, this.g.addEdge(this.v, this.x, 2));
        assertEquals(false, this.g.addEdge(this.v, this.u, 1));
        assertEquals(false, this.g.addEdge(this.edgeVX));
        assertEquals(3, this.g.numVerts());
        assertEquals(2, this.g.numEdges());
        assertEquals(true, this.g.addEdge(this.edgeAB));
        assertEquals(true, this.g.addEdge(this.edgeAX));
        assertEquals(5, this.g.numVerts());
        assertEquals(4, this.g.numEdges());
        this.g.clear();
    }

    @Test
    public void deleteEdge() {
        assertEquals(false, this.g.deleteEdge(this.a, this.b));
        this.addSetUp();
        assertEquals(true, this.g.areAdjacent(this.a, this.b));
        assertEquals(true, this.g.deleteEdge(this.a, this.b));
        assertEquals(false, this.g.deleteEdge(this.a, this.b));
        assertEquals(false, this.g.deleteEdge(this.u, this.x));
        assertEquals(4, this.g.numEdges());
        this.g.clear();
    }

    @Test
    public void testAdjacency() {
        assertEquals(false, this.g.areAdjacent(this.a, this.b));
        this.g.addVertex(this.v);
        this.g.addVertex(this.u);
        this.g.addVertex(this.x);
        this.g.addVertex(this.y);
        assertEquals(false, this.g.areAdjacent(this.u, this.v));
        this.g.addEdge(this.edgeVU);
        this.g.addEdge(this.edgeVX);
        assertEquals(true, this.g.areAdjacent(this.u, this.v));
        assertEquals(true, this.g.areAdjacent(this.v, this.u));
        assertEquals(true, this.g.areAdjacent(this.v, this.x));
        assertEquals(false, this.g.areAdjacent(this.x, this.u));
        assertEquals(false, this.g.areAdjacent(this.v, this.y));
        this.g.clear();
    }

    @Test
    public void testIncidence() {
        //take to ta's. Does incidence mean next to each other in the map
        //or just by definition assertFalse(this.g.areIncident(edgeAB, a));
        this.g.addVertex(this.v);
        this.g.addVertex(this.u);
        this.g.addVertex(this.x);
        this.g.addVertex(this.y);
        this.g.addEdge(this.edgeVU);
        //assertEquals(false, this.g.areIncident(edgeVX, x));
        assertEquals(false, this.g.areIncident(this.edgeVX, this.y));
        assertEquals(true, this.g.areIncident(this.edgeVX, this.v));
        assertFalse(this.g.areIncident(this.edgeVX, this.u));
        this.g.addEdge(this.edgeVX);
        assertFalse(this.g.areIncident(this.edgeVU, this.x));
        assertTrue(this.g.areIncident(this.edgeVU, this.u));
        assertEquals(4, this.g.numVerts());
        assertEquals(2, this.g.numEdges());
        this.g.clear();
    }

    @Test
    public void testDegree() {
        this.g.addVertex(this.v);
        this.g.addVertex(this.u);
        this.g.addVertex(this.x);
        this.g.addVertex(this.y);
        assertEquals(0, this.g.degree(this.v));
        this.g.addEdge(this.edgeVU);
        assertEquals(1, this.g.degree(this.v));
        this.g.addEdge(this.edgeVX);
        assertEquals(2, this.g.degree(this.v));
        assertEquals(1, this.g.degree(this.x));
        assertEquals(0, this.g.degree(this.y));
        this.g.clear();
    }


    @Test
    public void testNeighbors() {
        this.g.addVertex(this.v);
        this.g.addVertex(this.u);
        this.g.addVertex(this.x);
        this.g.addVertex(this.y);
        assertTrue(this.g.neighbors(this.v).isEmpty());
        this.g.addEdge(this.edgeVX);
        //System.out.println(this.g.neighbors(v).toString());
        assertTrue(this.g.neighbors(this.v).contains(this.x));
        assertFalse(this.g.neighbors(this.u).contains(this.v));
        this.addSetUp();
        assertTrue(this.g.neighbors(this.u).contains(this.v));
        assertTrue(this.g.neighbors(this.v).contains(this.u));
        assertTrue(this.g.neighbors(this.v).contains(this.b));
        assertFalse(this.g.neighbors(this.y).contains(this.v));
        this.g.clear();
    }

    @Test
    public void testAllEdges() {
        assertTrue(this.g.allEdges().isEmpty());
        this.g.addVertex(this.v);
        this.g.addVertex(this.u);
        assertFalse(this.g.allEdges().contains(this.edgeVU));
        this.g.addEdge(this.edgeVU);
        assertTrue(this.g.allEdges().contains(this.edgeVU));
        this.addSetUp();
        assertTrue(this.g.allEdges().contains(this.edgeAB));
        assertTrue(this.g.allEdges().contains(this.edgeAX));
        assertTrue(this.g.allEdges().contains(this.edgeBV));
        assertTrue(this.g.allEdges().contains(this.edgeVU));
        assertTrue(this.g.allEdges().contains(this.edgeAX));
        this.g.deleteEdge(this.a, this.b);
        assertFalse(this.g.allEdges().contains(this.edgeAB));
        this.g.clear();
    }

    @Test
    public void testAllVertices() {
        assertTrue(this.g.allVertices().isEmpty());
        this.g.addVertex(this.v);
        assertTrue(this.g.allVertices().contains(this.v));
        assertFalse(this.g.allVertices().contains(this.u));
        this.addSetUp();
        assertTrue(this.g.allVertices().contains(this.a));
        assertTrue(this.g.allVertices().contains(this.b));
        assertTrue(this.g.allVertices().contains(this.v));
        assertTrue(this.g.allVertices().contains(this.u));
        assertTrue(this.g.allVertices().contains(this.x));
        assertTrue(this.g.allVertices().contains(this.y));
        this.g.clear();
    }

    @Test
    public void testIncidentEdges() {
        this.g.addVertex(this.a);
        this.g.addVertex(this.b);
        this.g.addEdge(this.edgeAB);
        assertTrue(this.g.incidentEdges(this.a).contains(this.edgeAB));
        this.addSetUp();
        assertTrue(this.g.incidentEdges(this.y).isEmpty());
        assertTrue(this.g.incidentEdges(this.v).contains(this.edgeBV));
        assertTrue(this.g.incidentEdges(this.v).contains(this.edgeVX));
        assertTrue(this.g.incidentEdges(this.v).contains(this.edgeVU));
        assertFalse(this.g.incidentEdges(this.a).contains(this.edgeVU));
        this.g.clear();
    }

    @Test
    public void testDepthFirstSingle() {
        this.g.addVertex(this.v);
        List<GVertex<String>> tester = this.g.depthFirst(this.v);
        assertEquals(0, tester.get(0).compareTo(this.v));
        assertEquals(1, tester.size());
        this.g.clear();

    }

    @Test
    public void testDepthFirstIso() {
        this.addSetUp();
        List<GVertex<String>> tester = this.g.depthFirst(this.y);
        assertEquals(0, tester.get(0).compareTo(this.y));
        assertEquals(1, tester.size());
        this.g.clear();
    }

    @Test
    public void testDepthFirstSeperate() {
        this.g.addVertex(this.a);
        this.g.addVertex(this.b);
        this.g.addVertex(this.v);
        this.g.addVertex(this.u);
        this.g.addVertex(this.x);
        this.g.addEdge(this.edgeVU);
        this.g.addEdge(this.edgeVX);
        this.g.addEdge(this.edgeAB);
        List<GVertex<String>> tester = this.g.depthFirst(this.a);
        assertEquals(0, tester.get(0).compareTo(this.a));
        assertEquals(0, tester.get(1).compareTo(this.b));
        assertEquals(2, tester.size());

        tester = this.g.depthFirst(this.v);
        assertEquals(0, tester.get(0).compareTo(this.v));
        assertEquals(0, tester.get(1).compareTo(this.u));
        assertEquals(0, tester.get(2).compareTo(this.x));
        assertEquals(3, tester.size());

        tester = this.g.depthFirst(this.x);
        assertEquals(0, tester.get(0).compareTo(this.x));
        assertEquals(0, tester.get(1).compareTo(this.v));
        assertEquals(0, tester.get(2).compareTo(this.u));
        assertEquals(3, tester.size());

        this.g.clear();
    }

    @Test
    public void testDepthFirstBig() {
        this.addSetUp();
        List<GVertex<String>> tester = this.g.depthFirst(this.v);
        assertEquals(0, tester.get(0).compareTo(this.v));
        assertEquals(0, tester.get(1).compareTo(this.u));
        assertEquals(0, tester.get(2).compareTo(this.x));
        assertEquals(0, tester.get(3).compareTo(this.a));
        assertEquals(0, tester.get(4).compareTo(this.b));
        assertEquals(5, tester.size());
        tester = this.g.depthFirst(this.u);
        assertEquals(0, tester.get(0).compareTo(this.u));
        assertEquals(0, tester.get(1).compareTo(this.v));
        assertEquals(0, tester.get(2).compareTo(this.x));
        assertEquals(0, tester.get(3).compareTo(this.a));
        assertEquals(0, tester.get(4).compareTo(this.b));
        assertEquals(5, tester.size());
        tester = this.g.depthFirst(this.x);
        assertEquals(0, tester.get(0).compareTo(this.x));
        assertEquals(0, tester.get(1).compareTo(this.v));
        assertEquals(0, tester.get(2).compareTo(this.u));
        assertEquals(0, tester.get(3).compareTo(this.b));
        assertEquals(0, tester.get(4).compareTo(this.a));
        assertEquals(5, tester.size());
        tester = this.g.depthFirst(this.a);
        assertEquals(0, tester.get(0).compareTo(this.a));
        assertEquals(0, tester.get(1).compareTo(this.x));
        assertEquals(0, tester.get(2).compareTo(this.v));
        assertEquals(0, tester.get(3).compareTo(this.u));
        assertEquals(0, tester.get(4).compareTo(this.b));
        assertEquals(5, tester.size());
        tester = this.g.depthFirst(this.b);
        assertEquals(0, tester.get(0).compareTo(this.b));
        assertEquals(0, tester.get(1).compareTo(this.a));
        assertEquals(0, tester.get(2).compareTo(this.x));
        assertEquals(0, tester.get(3).compareTo(this.v));
        assertEquals(0, tester.get(4).compareTo(this.u));
        assertEquals(5, tester.size());
        this.g.clear();
    }

    @Test
    public void testKruskalsBig() {
        this.addSetUp();
        this.edgeAY = new WEdge<String>(this.a, this.y, 1);
        this.g.addEdge(this.edgeAY);
        List<WEdge<String>> tester = this.g.kruskals();
        assertEquals(0, tester.get(0).compareTo(this.edgeVU));
        assertEquals(0, tester.get(1).compareTo(this.edgeAY));
        assertEquals(0, tester.get(2).compareTo(this.edgeVX));
        assertEquals(0, tester.get(3).compareTo(this.edgeBV));
        assertEquals(0, tester.get(4).compareTo(this.edgeAX));
        assertEquals(5, tester.size());
    }

    @Test
    public void testKruskalsSame() {
        WGraphP4<String> gr = new WGraphP4<String>();
        GVertex<String> c, d, e, f, h, i;
        WEdge<String> edgeCD, edgeCE, edgeEF, edgeIF, edgeDI, edgeDH, edgeEI;

        c = new GVertex<String>("0", gr.nextID());
        d = new GVertex<String>("1", gr.nextID());
        e = new GVertex<String>("2", gr.nextID());
        f = new GVertex<String>("3", gr.nextID());
        h = new GVertex<String>("4", gr.nextID());
        i = new GVertex<String>("5", gr.nextID());
        edgeCD = new WEdge<String>(c, d, 1);
        edgeCE = new WEdge<String>(c, e, 1);
        edgeEF = new WEdge<String>(e, f, 1);
        edgeDI = new WEdge<String>(d, i, 1);
        edgeIF = new WEdge<String>(i, f, 1);
        edgeDH = new WEdge<String>(d, h, 1);
        edgeEI = new WEdge<String>(e, i, 1);

        gr.addVertex(c);
        gr.addVertex(d);
        gr.addVertex(h);
        gr.addVertex(f);
        gr.addVertex(i);
        gr.addVertex(e);
        gr.addEdge(edgeCD);
        gr.addEdge(edgeCE);
        gr.addEdge(edgeEF);
        gr.addEdge(edgeDI);
        gr.addEdge(edgeIF);
        gr.addEdge(edgeDH);
        gr.addEdge(edgeEI);

        List<WEdge<String>> tester = gr.kruskals();
        assertEquals(0, tester.get(0).compareTo(edgeCD));
        assertEquals(0, tester.get(1).compareTo(edgeEI));
        assertEquals(0, tester.get(2).compareTo(edgeDH));
        assertEquals(0, tester.get(3).compareTo(edgeIF));
        assertEquals(0, tester.get(4).compareTo(edgeDI));
        assertEquals(5, tester.size());

    }

    @Test
    public void testKruskalsUnique() {
        WGraphP4<String> gr = new WGraphP4<String>();
        GVertex<String> c, d, e, f, h, i;
        WEdge<String> edgeCD, edgeCE, edgeEF, edgeIF, edgeDI, edgeDH, edgeEI;

        c = new GVertex<String>("0", gr.nextID());
        d = new GVertex<String>("1", gr.nextID());
        e = new GVertex<String>("2", gr.nextID());
        f = new GVertex<String>("3", gr.nextID());
        h = new GVertex<String>("4", gr.nextID());
        i = new GVertex<String>("5", gr.nextID());
        edgeCD = new WEdge<String>(c, d, 1);
        edgeCE = new WEdge<String>(c, e, 2);
        edgeEF = new WEdge<String>(e, f, 3);
        edgeDI = new WEdge<String>(d, i, 4);
        edgeIF = new WEdge<String>(i, f, 5);
        edgeDH = new WEdge<String>(d, h, 6);
        edgeEI = new WEdge<String>(e, i, 7);

        gr.addVertex(c);
        gr.addVertex(d);
        gr.addVertex(h);
        gr.addVertex(f);
        gr.addVertex(i);
        gr.addVertex(e);
        gr.addEdge(edgeCD);
        gr.addEdge(edgeCE);
        gr.addEdge(edgeEF);
        gr.addEdge(edgeDI);
        gr.addEdge(edgeIF);
        gr.addEdge(edgeDH);
        gr.addEdge(edgeEI);

        List<WEdge<String>> tester = gr.kruskals();
        assertEquals(0, tester.get(0).compareTo(edgeCD));
        assertEquals(0, tester.get(1).compareTo(edgeCE));
        assertEquals(0, tester.get(2).compareTo(edgeEF));
        assertEquals(0, tester.get(3).compareTo(edgeDI));
        assertEquals(0, tester.get(4).compareTo(edgeDH));
        assertEquals(5, tester.size());

    }

    @Test
    public void testKruskalsMixed() {
        WGraphP4<String> gr = new WGraphP4<String>();
        GVertex<String> c, d, e, f, h, i;
        WEdge<String> edgeCD, edgeCE, edgeEF, edgeIF, edgeDI, edgeDH, edgeEI,
            edgeDF;

        c = new GVertex<String>("0", gr.nextID());
        d = new GVertex<String>("1", gr.nextID());
        e = new GVertex<String>("2", gr.nextID());
        f = new GVertex<String>("3", gr.nextID());
        h = new GVertex<String>("4", gr.nextID());
        i = new GVertex<String>("5", gr.nextID());
        edgeCD = new WEdge<String>(c, d, 1);
        edgeCE = new WEdge<String>(c, e, 3);
        edgeEF = new WEdge<String>(e, f, 4);
        edgeDI = new WEdge<String>(d, i, 1);
        edgeIF = new WEdge<String>(i, f, 2);
        edgeDH = new WEdge<String>(d, h, 3);
        edgeEI = new WEdge<String>(e, i, 4);
        edgeDF = new WEdge<String>(d, f, 2);

        gr.addVertex(c);
        gr.addVertex(d);
        gr.addVertex(h);
        gr.addVertex(f);
        gr.addVertex(i);
        gr.addVertex(e);
        gr.addEdge(edgeCD);
        gr.addEdge(edgeCE);
        gr.addEdge(edgeEF);
        gr.addEdge(edgeDI);
        gr.addEdge(edgeIF);
        gr.addEdge(edgeDH);
        gr.addEdge(edgeEI);
        gr.addEdge(edgeDF);

        List<WEdge<String>> tester = gr.kruskals();
        assertEquals(0, tester.get(0).compareTo(edgeCD));
        assertEquals(0, tester.get(1).compareTo(edgeDI));
        assertEquals(0, tester.get(2).compareTo(edgeIF));
        assertEquals(0, tester.get(3).compareTo(edgeDH));
        assertEquals(0, tester.get(4).compareTo(edgeCE));
        assertEquals(5, tester.size());

    }

    @Test
    public void testKruskalsNecessaryLarge() {
        WGraphP4<String> gr = new WGraphP4<String>();
        GVertex<String> c, d, e, f, h, i;
        WEdge<String> edgeCD, edgeCE, edgeDE, edgeDI, edgeIF, edgeHI, edgeFH;

        c = new GVertex<String>("0", gr.nextID());
        d = new GVertex<String>("1", gr.nextID());
        e = new GVertex<String>("2", gr.nextID());
        f = new GVertex<String>("3", gr.nextID());
        h = new GVertex<String>("4", gr.nextID());
        i = new GVertex<String>("5", gr.nextID());
        edgeCD = new WEdge<String>(c, d, 1);
        edgeCE = new WEdge<String>(c, e, 1);
        edgeDE = new WEdge<String>(d, e, 2);
        edgeDI = new WEdge<String>(d, i, 3);
        edgeIF = new WEdge<String>(i, f, 2);
        edgeHI = new WEdge<String>(h, i, 1);
        edgeFH = new WEdge<String>(f, h, 1);

        gr.addVertex(c);
        gr.addVertex(d);
        gr.addVertex(h);
        gr.addVertex(f);
        gr.addVertex(i);
        gr.addVertex(e);
        gr.addEdge(edgeCD);
        gr.addEdge(edgeCE);
        gr.addEdge(edgeDE);
        gr.addEdge(edgeDI);
        gr.addEdge(edgeIF);
        gr.addEdge(edgeHI);
        gr.addEdge(edgeFH);

        List<WEdge<String>> tester = gr.kruskals();
        assertEquals(0, tester.get(0).compareTo(edgeCD));
        assertEquals(0, tester.get(1).compareTo(edgeFH));
        assertEquals(0, tester.get(2).compareTo(edgeHI));
        assertEquals(0, tester.get(3).compareTo(edgeCE));
        assertEquals(0, tester.get(4).compareTo(edgeDI));
        assertEquals(5, tester.size());

    }


}
