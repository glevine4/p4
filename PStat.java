/**
 * Gregory Levine <glevine4@jhu.edu>
 * Matthew Saltzman <msaltzm5@jhu.edu>
 * Buck Thompson <bthomp36@jhu.edu>
 * 600.226 Data Structures
 * Assignment P4
 *
 * Keeps track of the max and min color values of each color for all pixels in
 * a partition.
 */
public class PStat {

    /** The minimum r value in partition. */
    private int rMin;
    /** The max r value in partition. */
    private int rMax;
    /** The minimum g value in partition. */
    private int gMin;
    /** The max g value in partition. */
    private int gMax;
    /** The minimum b value in partition. */
    private int bMin;
    /** The max b value in partition. */
    private int bMax;

    /**
    * A constructor that is called on the first pixel in the partition.
    * @param p the first pixel.
    */
    public PStat(Pixel p) {
        this.setrMin(p.r);
        this.setrMax(p.r);
        this.setgMin(p.g);
        this.setgMax(p.g);
        this.setbMin(p.g);
        this.setbMax(p.g);
    }

    /**
    * Returns the minimum red Value in the partition.
    * @return the value.
    */
    public int getrMin() {
        return this.rMin;
    }

    /**
    * Updates the minimum red Value in the partition.
    * @param r the new red value.
    */
    public void setrMin(int r) {
        this.rMin = r;
    }

    /**
    * Returns the max red Value in the partition.
    * @return the value.
    */
    public int getrMax() {
        return this.rMax;
    }

    /**
    * Updates the max red Value in the partition.
    * @param r the value.
    */
    public void setrMax(int r) {
        this.rMax = r;
    }

    /**
    * Returns the minimum green Value in the partition.
    * @return the value.
    */
    public int getgMin() {
        return this.gMin;
    }

    /**
    * Updates the minimum green Value in the partition.
    * @param g the new green value.
    */
    public void setgMin(int g) {
        this.gMin = g;
    }

    /**
    * Returns the max green Value in the partition.
    * @return the value.
    */
    public int getgMax() {
        return this.gMax;
    }

    /**
    * Updates the max green Value in the partition.
    * @param g the new green value.
    */
    public void setgMax(int g) {
        this.gMax = g;
    }

    /**
    * Returns the minimum blue Value in the partition.
    * @return the value.
    */
    public int getbMin() {
        return this.bMin;
    }

    /**
    * Updates the minimum blue Value in the partition.
    * @param b the new blue value.
    */
    public void setbMin(int b) {
        this.bMin = b;
    }

    /**
    * Returns the max blue Value in the partition.
    * @return the value.
    */
    public int getbMax() {
        return this.bMax;
    }

    /**
    * Updates the max blue Value in the partition.
    * @param b the new blue value.
    */
    public void setbMax(int b) {
        this.bMax = b;
    }
}
