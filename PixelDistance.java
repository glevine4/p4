/**
 * Gregory Levine <glevine4@jhu.edu>
 * Matthew Saltzman <msaltzm5@jhu.edu>
 * Buck Thompson <bthomp36@jhu.edu>
 * 600.226 Data Structures
 * Assignment P4
 *
 * Implements the distance interface for Pixel objects.
 */
public class PixelDistance implements Distance<Pixel> {

    /**
     * Given two pixels, gives the "distance" between them.
     * @param one the first pixel.
     * @param two the second pixel.
     * @return the distance.
     */
    @Override
    public double distance(Pixel one, Pixel two) {
        return Math.pow(one.r - two.r, 2) + Math.pow(one.g - two.g, 2)
            + Math.pow(one.b - two.b, 2);
    }
}
