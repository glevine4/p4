import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.HashMap;
import java.util.Scanner;

/**
 * Gregory Levine <glevine4@jhu.edu>
 * Matthew Saltzman <msaltzm5@jhu.edu>
 * Buck Thompson <bthomp36@jhu.edu>
 * 600.226 Data Structures
 * Assignment P4
 *
 * The main class for P4C.
 */
public final class P4C {

    /**
     * Hides Constructor.
     */
    private P4C() {
    }

    /**
     * Convert an image to a graph of Pixels with edges between north, south,
     * east and west neighboring pixels.
     * @param image the image to convert
     * @param pd the distance object for pixels
     * @return the graph that was created
     */
    static WGraphP4<Pixel> imageToGraph(BufferedImage image,
        Distance<Pixel> pd) {

        //Create a new graph, get the height and width of image
        WGraphP4<Pixel> graph = new WGraphP4<Pixel>();
        int cols = image.getWidth();
        int rows = image.getHeight();
        //Create some temporary vertex holders
        GVertex<Pixel> rt;
        GVertex<Pixel> dn;
        GVertex<Pixel> cr;
        //Create an array to hold the vertices cooresponding to pixels in image
        @SuppressWarnings("unchecked")
        GVertex<Pixel>[][] vArray = new GVertex[rows][cols];
        Pixel p;

        // Populate 2d array with vertexes holding pixels so that they may be
        // referenced during edge creation loops
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                p = new Pixel(i, j, image.getRGB(j, i));
                cr = new GVertex<Pixel>(p, graph.nextID());
                vArray[i][j] = cr;
            }
        }

        // Edge creation loop iterating over vArray of GVertex<Pixel> elements
        // created in last loop
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols - 1; j++) {
                cr = vArray[i][j];
                // case bottom row
                if (i == rows - 1) {
                    rt = vArray[i][j + 1];
                    graph.addEdge(cr, rt, pd.distance(cr.value(), rt.value()));
                } else {
                    rt = vArray[i][j + 1];
                    dn = vArray[i + 1][j];
                    graph.addEdge(cr, rt, pd.distance(cr.value(), rt.value()));
                    graph.addEdge(cr, dn, pd.distance(cr.value(), dn.value()));
                }
            }
            // case far right edge
            if (i != rows - 1) {
                cr = vArray[i][cols - 1];
                dn = vArray[i + 1][cols - 1];
                graph.addEdge(cr, dn, pd.distance(cr.value(), dn.value()));
            }
        }
        return graph;
    }

    /**
     * Returns a partition of the vertices that represents the segmentation of
     * the image according to the image segmentation algorithm.
     * @param g the graph to segment
     * @param kvalue the value to use for k in the merge test
     * @return a partition of the vertices in the graph.
     */
    static Partition segmenter(WGraphP4<Pixel> g, double kvalue) {
        //Create a PQ of the edges in the graph, to sort by weight
        PQHeap<WEdge<Pixel>> queue = new PQHeap<WEdge<Pixel>>();
        queue.init(g.allEdges());
        //Create an array of PStat objects, to keep track of max and min color
        //values for each segment.
        PStat[] stats = populateStats(g);
        //The partiton of the vertices
        Partition p = new Partition(g.numVerts());
        int sourceParent;
        int endParent;
        int sourceID;
        int endID;
        int i = 0;
        //For each edge in the graph, decides whether it should be included in
        //the minimum spanning forest, using Kruscals algorithm and the
        //additional merge condition.
        while (i < g.numVerts() - 1 && !queue.isEmpty()) {
            WEdge<Pixel> edge = queue.remove();
            sourceID = edge.source().id();
            endID = edge.end().id();
            sourceParent = p.find(sourceID);
            endParent = p.find(endID);
            if ((sourceParent != endParent) && jB(stats[sourceParent],
                stats[endParent], p.getWeight(sourceParent),
                p.getWeight(endParent), kvalue)) {

                p.union(sourceID, endID);
                combineStats(stats[sourceParent], stats[endParent]);
                i++;
            }
        }
        return p;
    }

    /**
     * The main function for P4C.
     * @param args the command line arguments.
     */
    public static void main(String[] args) {
        if (args.length != 2) {
            System.err.println("Invalid command line args.");
            System.exit(0);
        }
        try {
            System.out.println("Reading image file...");
            BufferedImage image = ImageIO.read(new File(args[0]));
            System.out.println("Building Graph...");
            WGraphP4<Pixel> g = imageToGraph(image, new PixelDistance());
            System.out.println("Segmenting Graph");
            Partition p = segmenter(g, Double.parseDouble(args[1]));
            //Give some stats
            System.out.print("Pixels in image: " + g.numVerts() + "\n");
            System.out.print("Number of Segments found =  "
                + (p.size()) + "\n");
            System.out.println("Continue?: y/n");
            Scanner kb = new Scanner(System.in);
            if (!kb.nextLine().equals("y")) {
                System.exit(0);
            }

            System.out.println("Creating segment lists...");
            List<List<GVertex<Pixel>>> segmentVerts = segmentVerts(g, p);

            System.out.println("Creating output files:");
            for (int j = 0; j < segmentVerts.size(); j++) {
                System.out.println("Creating image " + (j + 1));
                greyify(image);
                for (GVertex<Pixel> i: segmentVerts.get(j)) {
                    Pixel d = i.value();
                    image.setRGB(d.col(), d.row(), d.value());
                }
                System.out.println("Writing file");
                File f = new File("output" + j + ".png");
                ImageIO.write(image, "png", f);
            }
            System.out.println("All done!");
        } catch (IOException e) {
            System.out.print("Invalid command line args.\n");
        }
    }

    /**
     * Combines the PStat of 2 partitions.
     * @param a the first PStat.
     * @param b the second PStat.
     */
    public static void combineStats(PStat a, PStat b) {
        a.setbMax(Math.max(a.getbMax(), b.getbMax()));
        b.setbMax(a.getbMax());
        a.setgMax(Math.max(a.getgMax(), b.getgMax()));
        b.setgMax(a.getgMax());
        a.setrMax(Math.max(a.getrMax(), b.getrMax()));
        b.setrMax(a.getrMax());
    }

    /**
     * Creates an array of PStats for the graph g.
     * @param g the graph.
     * @return the array.
     */
    public static PStat[] populateStats(WGraphP4<Pixel> g) {
        PStat[] list = new PStat[g.numVerts()];
        Iterator<GVertex<Pixel>> iter = g.allVertices().iterator();
        GVertex<Pixel> vert;
        while (iter.hasNext()) {
            vert = iter.next();
            list[vert.id()] = new PStat(vert.value());
        }
        return list;
    }

    /**
     * Tests to see if the two partitions should be merged.
     * @param a the PStat for the first partition.
     * @param b the PStat for the second partition.
     * @param aWeight the weight of the first partition.
     * @param bWeight the weight of the second partition.
     * @param k the constant k to use.
     * @return true if the partitions should be merged.
     */
    public static boolean jB(PStat a, PStat b, int aWeight, int bWeight,
        double k) {

        return (testRed(a, b, aWeight, bWeight, k) && testGreen(a, b, aWeight,
            bWeight, k) && testBlue(a, b, aWeight, bWeight, k));
    }
    /**
     * Tests to see if the two partitions should be merged.
     * @param a the PStat for the first partition.
     * @param b the PStat for the second partition.
     * @param aWeight the weight of the first partition.
     * @param bWeight the weight of the second partition.
     * @param k the constant k to use.
     * @return true if the partitions should be merged.
     */
    public static boolean testRed(PStat a, PStat b, int aWeight, int bWeight,
        double k) {

        int fMax = Math.max(a.getrMax(), b.getrMax());
        int fMin = Math.min(a.getrMin(), b.getrMin());
        int aDiff = a.getrMax() - a.getrMin();
        int bDiff = b.getrMax() - b.getrMin();
        int fDiff = fMax - fMin;
        double condition = (Math.min(aDiff, bDiff) + (k / (aWeight + bWeight)));
        return fDiff <= condition;
    }

    /**
     * Tests to see if the two partitions should be merged.
     * @param a the PStat for the first partition.
     * @param b the PStat for the second partition.
     * @param aWeight the weight of the first partition.
     * @param bWeight the weight of the second partition.
     * @param k the constant k to use.
     * @return true if the partitions should be merged.
     */
    public static boolean testGreen(PStat a, PStat b, int aWeight, int bWeight,
        double k) {

        int fMax = Math.max(a.getgMax(), b.getgMax());
        int fMin = Math.min(a.getgMin(), b.getgMin());
        int aDiff = a.getgMax() - a.getgMin();
        int bDiff = b.getgMax() - b.getgMin();
        int fDiff = fMax - fMin;
        double condition = (Math.min(aDiff, bDiff) + (k / (aWeight + bWeight)));
        return fDiff <= condition;
    }

    /**
     * Tests to see if the two partitions should be merged.
     * @param a the PStat for the first partition.
     * @param b the PStat for the second partition.
     * @param aWeight the weight of the first partition.
     * @param bWeight the weight of the second partition.
     * @param k the constant k to use.
     * @return true if the partitions should be merged.
     */
    public static boolean testBlue(PStat a, PStat b, int aWeight, int bWeight,
        double k) {

        int fMax = Math.max(a.getbMax(), b.getbMax());
        int fMin = Math.min(a.getbMin(), b.getbMin());
        int aDiff = a.getbMax() - a.getbMin();
        int bDiff = b.getbMax() - b.getbMin();
        int fDiff = fMax - fMin;
        double condition = (Math.min(aDiff, bDiff) + (k / (aWeight + bWeight)));
        return fDiff <= condition;
    }

    /**
     * Creates an ArrayList with one GVertex in each tree of a spanning forest.
     * @param g the graph.
     * @param p the partition.
     * @return a list of pixels, one per grouping.
     */
    public static List<List<GVertex<Pixel>>> segmentVerts(WGraphP4<Pixel> g,
        Partition p) {

        //Each list in the list is for one segment of the graph.
        List<List<GVertex<Pixel>>> segmentVerts =
            new ArrayList<List<GVertex<Pixel>>>();
        //Maps the parent node id number for a segment to an index in
        //segmentVerts
        Map<Integer, Integer> index = new HashMap<Integer, Integer>();
        for (int i = 0; i < p.size(); i++) {
            segmentVerts.add(new ArrayList<GVertex<Pixel>>());
        }
        //For all vertices in the graph, add them to the appropriate list in
        //segmentVerts based on the partiton.
        for (GVertex<Pixel> vert : g.allVertices()) {
            int parent = p.find(vert.id());
            Integer i = index.get(parent);
            if (i == null) {
                index.put(parent, index.size());
                i = index.size() - 1;
            }
            segmentVerts.get(i).add(vert);
        }
        return segmentVerts;
    }

    /**
     * Fills in a BufferedImage with gray.
     * @param image the image.
     */
    public static void greyify(BufferedImage image) {
        final int gray = 0x606060;
        for (int i = 0; i < image.getHeight(); i++) {
            for (int b = 0; b < image.getWidth(); b++) {
                image.setRGB(b, i, gray);
            }
        }
    }

}
