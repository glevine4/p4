/**
 * Gregory Levine <glevine4@jhu.edu>
 * Matthew Saltzman <msaltzm5@jhu.edu>
 * Buck Thompson <bthomp36@jhu.edu>
 * 600.226 Data Structures
 * Assignment P4
 *
 * Represents a single pixel of an image.
 */
public class Pixel {

    /** A mask for the lower byte of an integer. */
    public static final int LOWER_BYTE_MASK = 0xFF;

    /** The number of bits to shift data to get green value. */
    public static final int GREEN_SHIFT = 8;

    /** The number of bits ro shift data to get blue value. */
    public static final int BLUE_SHIFT = 16;

    /** An integer whose bytes represent the rgb color values. */
    public int data;

    /** The row of the pixel within the image. */
    public int row;

    /** The column of the pixel within the image. */
    public int col;

    /** The red color value of the pixel. */
    public int r;

    /** The green color value of the pixel. */
    public int g;

    /** The blue color value of the pixel. */
    public int b;

    /**
     * Constructor for the pixel.
     * @param rowVal the row.
     * @param colVal the column.
     * @param rgb an integer whose bytes represent the rgb color values.
     */
    public Pixel(int rowVal, int colVal, int rgb) {
        this.data = rgb;
        this.row = rowVal;
        this.col = colVal;
        this.b = rgb & this.LOWER_BYTE_MASK;
        this.g = (rgb >> this.GREEN_SHIFT) & this.LOWER_BYTE_MASK;
        this.b = (rgb >> this.BLUE_SHIFT) & this.LOWER_BYTE_MASK;
    }

    /**
     * Returns the row.
     * @return row.
     */
    public int row() {
        return this.row;
    }

    /**
     * Return the column.
     * @return the column.
     */
    public int col() {
        return this.col;
    }

    /**
     * Return the integer representing the rgb color values.
     * @return the integer.
     */
    public int value() {
        return this.data;
    }
}
