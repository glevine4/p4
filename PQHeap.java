import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;

/**
  * Gregory Levine <glevine4@jhu.edu>
  * Matthew Saltzman <msaltzm5@jhu.edu>
  * Buck Thompson <bthomp36@jhu.edu>
  * 600.226 Data Structures
  * Assignment P4
  * A Heap implementation of the Maximum Priority Queue datastructure.
  * @param <T> the type of element stored in the Queue.
  */
public class PQHeap<T extends Comparable<? super T>> implements
    PriorityQueue<T> {

    /** The array which holds the heap. */
    private ArrayList<T> heap = new ArrayList<T>();

    /** Stores the comparator that will be used for maintaining the heap. */
    private Comparator<T> c;

    /**
     * Default constructor that uses the natural comparator. Will result in a
     * min heap.
     */
    public PQHeap() {
        this.c = new Comparator<T>() {
            public int compare(T object1, T object2) {
                return object1.compareTo(object2);
            }
        };
    }

    /**
     * Constructor that takes a comparator to use for the queue.
     * @param comp the comparator.
     */
    public PQHeap(Comparator<T> comp) {
        this.c = comp;
    }

    /**
     * Get the number of elements in the priority queue.
     * @return the size
     */
    @Override
    public int size() {
        return this.heap.size();
    }

    /**
     * Determine if the priority queue is empty or not.
     * @return true if empty, false otherwise
     */
    @Override
    public boolean isEmpty() {
        return this.heap.size() == 0;
    }

    /**
     * Dump the contents of the priority queue.
     */
    @Override
    public void clear() {
        this.heap = new ArrayList<T>();
    }

    /**
     * Find out the maximum value in the queue (no removal).
     * @return the max value (throw exception if empty)
     */
    @Override
    public T peek() throws QueueEmptyException {
        if (this.isEmpty()) {
            throw new QueueEmptyException();
        }
        return this.heap.get(0);
    }

    /**
     * Remove the maximum value in the queue.
     * @return the value removed (throw exception if empty)
     */
    @Override
    public T remove() throws QueueEmptyException {
        if (this.isEmpty()) {
            throw new QueueEmptyException();
        }
        T max = this.heap.get(0);
        T last = this.heap.remove(this.heap.size() - 1);
        if (this.heap.size() == 0) {
            return max;
        }
        this.heap.set(0, last);
        int i = 0;
        while (2 * i + 1 < this.heap.size()) {
            if (this.c.compare(this.heap.get(i), this.heap.get(2 * i + 1))
                > 0) {
                i = this.swap(i);
            } else if (((2 * i + 2) < this.heap.size())
                && this.c.compare(this.heap.get(i),
                this.heap.get(2 * i + 2)) > 0) {
                i = this.swap(i);
            } else {
                break;
            }
        }
        return max;
    }

    /**
     * Swaps the node at index i with the appropriate child.
     * @param i the index.
     * @return the index of the child.
     */
    private int swap(int i) {
        if (2 * i + 2 >= this.heap.size()) {
            T temp = this.heap.get(i);
            this.heap.set(i, this.heap.get(2 * i + 1));
            this.heap.set(2 * i + 1, temp);
            return 2 * i + 1;
        }
        if (this.c.compare(this.heap.get(2 * i + 1), this.heap.get(2 * i + 2))
            < 0) {
            T temp = this.heap.get(i);
            this.heap.set(i, this.heap.get(2 * i + 1));
            this.heap.set(2 * i + 1, temp);
            return 2 * i + 1;
        } else {
            T temp = this.heap.get(i);
            this.heap.set(i, this.heap.get(2 * i + 2));
            this.heap.set(2 * i + 2, temp);
            return 2 * i + 2;
        }
    }

    /**
     * Add a value to the priority queue.
     * @param val the value to be added (duplicates ok)
     */
    @Override
    public void insert(T val) {
        this.heap.add(val);
        int i = this.heap.size() - 1;
        while (i > 0) {
            if (this.c.compare(this.heap.get(i), this.heap.get((i - 1) / 2))
                < 0) {
                T temp = this.heap.get(i);
                this.heap.set(i, this.heap.get((i - 1) / 2));
                this.heap.set((i - 1) / 2, temp);
                i = (i - 1) / 2;
            } else {
                break;
            }
        }
    }

    /**
     * Initialize a priority queue from a container of values.
     * @param values the collection of starting values
     */
    @Override
    public void init(Collection<T> values) {
        this.heap = new ArrayList<T>();
        this.heap.addAll(values);
        for (int i = this.heap.size() / 2; i >= 0; i--) {
            this.heapify(i);
        }
    }

    /**
     * Moves elements in the subtree with root at element i to satisfy the heap
     * property.
     * @param i the index of the element.
     */
    private void heapify(int i) {
        if ((2 * i + 1) < this.heap.size() && this.c.compare(this.heap.get(i),
            this.heap.get(2 * i + 1)) > 0) {
            this.heapify(this.swap(i));

        } else if (((2 * i + 2) < this.heap.size())
            && this.c.compare(this.heap.get(i), this.heap.get(2 * i + 2)) > 0) {
            this.heapify(this.swap(i));
        }
    }
}
