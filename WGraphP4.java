
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.LinkedList;
import java.util.Iterator;

/**
  * Gregory Levine <glevine4@jhu.edu>
  * Matthew Saltzman <msaltzm5@jhu.edu>
  * Buck Thompson <bthomp36@jhu.edu>
  * 600.226 Data Structures
  * Assignment P4
  *
  * Implementation of a weighted generic graph.
  * @param <T> the type of data to store at each node.
  */
public class WGraphP4<T> implements WGraph<T> {

    /** A Map with the vertices as keys and a list of incident
    edges as values. */
    private Map<GVertex<T>, List<WEdge<T>>> adjacencyList;

    /** A list containing all the edges. */
    private List<WEdge<T>> edgeList;

    /** The next id ready to be assigned to a vertex. */
    private int nextID;

    /**
     * Constructor that initializes the graph.
     */
    public WGraphP4() {
        this.clear();
    }

    /**
     * Clears the graph.
     */
    public void clear() {
        this.adjacencyList = new HashMap<GVertex<T>, List<WEdge<T>>>();
        this.edgeList = new LinkedList<WEdge<T>>();
        this.nextID = 0;
    }

    /** Get the number of edges.
     *  @return the number
     */
    public int numEdges() {
        return this.edgeList.size();
    }

    /** Get the number of vertices.
     *  @return the number
     */
    public int numVerts() {
        return this.adjacencyList.size();
    }

    /** Get the next ID to use in making a vertex.
     *  @return the id
     */
    public int nextID() {
        this.nextID++;
        return this.nextID - 1;
    }

    /** Create and add a vertex to the graph.
     *  @param d the data to store in the vertex
     *  @return true if successful, false otherwise
     */
    public boolean addVertex(T d) {
        GVertex<T> node = new GVertex<T>(d, this.nextID());
        return this.addVertex(node);
    }

    /** Add a vertex if it doesn't exist yet.
     *  @param v the vertex to add
     *  @return false if already there, true if added
     */
    public boolean addVertex(GVertex<T> v) {
        if (this.adjacencyList.containsKey(v)) {
            return false;
        }
        if (v.id() >= this.nextID) {
            this.nextID = v.id() + 1;
        }
        List<WEdge<T>> list = new LinkedList<WEdge<T>>();
        this.adjacencyList.put(v, list);
        return true;
    }

    /** Add a weighted edge, may also add the incident vertices.
     *  @param e the edge to add
     *  @return false if already there, true if added
     */
    public boolean addEdge(WEdge<T> e) {
        if (!this.adjacencyList.containsKey(e.end())) {
            this.addVertex(e.end());
        }
        if (!this.adjacencyList.containsKey(e.source())) {
            this.addVertex(e.source());
        }
        if (this.adjacencyList.get(e.source()).contains(e)) {
            return false;
        }
        this.edgeList.add(e);
        this.adjacencyList.get(e.source()).add(e);
        this.adjacencyList.get(e.end()).add(e);
        return true;
    }

    /** Add a weighted edge, may also add vertices.
     *  @param v the starting vertex
     *  @param u the ending vertex
     *  @param weight the weight of the edge
     *  @return false if already there, true if added
     */
    public boolean addEdge(GVertex<T> v, GVertex<T> u, double weight) {
        WEdge<T> e = new WEdge<T>(v, u, weight);
        return this.addEdge(e);
    }

    /** Remove an edge if there.
     *  @param v the starting vertex
     *  @param u the ending vertex
     *  @return true if delete, false if wasn't there
     */
    public boolean deleteEdge(GVertex<T> v, GVertex<T> u) {
        if (this.adjacencyList.containsKey(v)
            && this.adjacencyList.containsKey(u)) {
            Iterator<WEdge<T>> iter = this.adjacencyList.get(v).iterator();
            while (iter.hasNext()) {
                WEdge<T> edge = iter.next();
                if (edge.source().equals(u) || edge.end().equals(u)) {
                    iter.remove();
                    this.adjacencyList.get(u).remove(edge);
                    this.edgeList.remove(edge);
                    return true;
                }
            }
        }
        return false;
    }

    /** Return true if there is an edge between v and u.
     *  @param v the starting vertex
     *  @param u the ending vertex
     *  @return true if there is an edge between them, false otherwise
     */
    public boolean areAdjacent(GVertex<T> v, GVertex<T> u) {
        if (!this.adjacencyList.containsKey(v)
            || !this.adjacencyList.containsKey(u)) {
            return false;
        }
        Iterator<WEdge<T>> iter = this.adjacencyList.get(v).iterator();
        while (iter.hasNext()) {
            WEdge<T> edge = iter.next();
            if (edge.source().equals(u) || edge.end().equals(u)) {
                return true;
            }
        }
        return false;
    }

    /** Return a list of all the neighbors of vertex v.
     *  @param v the vertex source
     *  @return the neighboring vertices
     */
    public List<GVertex<T>> neighbors(GVertex<T> v) {
        List<GVertex<T>> list = new LinkedList<GVertex<T>>();
        if (!this.adjacencyList.containsKey(v)) {
            return list;
        }
        Iterator<WEdge<T>> iter = this.adjacencyList.get(v).iterator();
        while (iter.hasNext()) {
            WEdge<T> edge = iter.next();
            if (edge.source().equals(v)) {
                list.add(edge.end());
            } else {
                list.add(edge.source());
            }
        }
        return list;
    }

    /** Return the number of edges incident to v.
     *  @param v the vertex source
     *  @return the number of incident edges
     */
    public int degree(GVertex<T> v) {
        List<WEdge<T>> list = this.adjacencyList.get(v);
        if (list == null) {
            return -1;
        }
        return list.size();
    }

    /** See if an edge and vertex are incident to each other.
     *  @param e the edge
     *  @param v the vertex to check
     *  @return true if v is an endpoint of edge e
     */
    public boolean areIncident(WEdge<T> e, GVertex<T> v) {
        if (e.source().equals(v) || e.end().equals(v)) {
            return true;
        }
        return false;
    }

    /** Return a list of all the edges.
     *  @return the list
     */
    public List<WEdge<T>> allEdges() {
        return new LinkedList<WEdge<T>>(this.edgeList);
    }

    /** Return a list of all the vertices.
     *  @return the list
     */
    public List<GVertex<T>> allVertices() {
        return new LinkedList<GVertex<T>>(this.adjacencyList.keySet());
    }

    /** Return a list of all the vertices that can be reached from v,
     *  in the order in which they would be visited in a depth-first
     *  search starting at v.
     *  @param v the starting vertex
     *  @return the list of reachable vertices
     */
    public List<GVertex<T>> depthFirst(GVertex<T> v) {
        boolean[] visited = new boolean[this.nextID];
        List<GVertex<T>> list = new LinkedList<GVertex<T>>();
        if (!this.adjacencyList.containsKey(v)) {
            return list;
        }
        this.depthFirst(v, visited, list);
        return list;
    }

    /** Return a list of all the vertices that can be reached from v,
     *  in the order in which they would be visited in a depth-first
     *  search starting at v. Takes a boolean array that indicates which
     *  vertices have already been reached.
     *  @param v the starting vertex
     *  @param visited the array of that indicated visited vertices.
     *  @param list the list of vertices in order.
     */
    public void depthFirst(GVertex<T> v, boolean[] visited,
        List<GVertex<T>> list) {

        visited[v.id()] = true;
        list.add(v);
        for (WEdge<T> edge : this.adjacencyList.get(v)) {
            GVertex<T> vertex = edge.source();
            if (vertex.equals(v)) {
                vertex = edge.end();
            }
            if (!visited[vertex.id()]) {
                this.depthFirst(vertex, visited, list);
            }
        }
    }

    /** Return a list of all the edges incident on vertex v.
     *  @param v the starting vertex
     *  @return the incident edges
     */
    public List<WEdge<T>> incidentEdges(GVertex<T> v) {
        return new LinkedList<WEdge<T>>(this.adjacencyList.get(v));
    }

    /** Return a list of edges in a minimum spanning forest by
     *  implementing Kruskal's algorithm using fast union/finds.
     *  @return a list of the edges in the minimum spanning forest
     */
    public List<WEdge<T>> kruskals() {
        PQHeap<WEdge<T>> queue = new PQHeap<WEdge<T>>();
        List<WEdge<T>> list = new LinkedList<WEdge<T>>();
        queue.init(this.allEdges());
        Partition p = new Partition(this.numVerts());
        while (list.size() < this.numVerts() - 1 && !queue.isEmpty()) {
            WEdge<T> edge = queue.remove();
            if (p.find(edge.source().id()) != p.find(edge.end().id())) {
                list.add(edge);
                p.union(edge.source().id(), edge.end().id());
            }
        }
        return list;
    }
}
