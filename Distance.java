/**
 * Gregory Levine <glevine4@jhu.edu>
 * Matthew Saltzman <msaltzm5@jhu.edu>
 * Buck Thompson <bthomp36@jhu.edu>
 * 600.226 Data Structures
 * Assignment P4
 *
 * Interface for a function that determines the distance between 2 objects.
 * @param <T> the object type.
 */
public interface Distance<T> {

    /**
    * Determines the distance between the objects.
    * @param one the first object.
    * @param two the second object.
    * @return the distance.
    */
    double distance(T one, T two);
}
