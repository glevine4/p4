/**
  * Gregory Levine <glevine4@jhu.edu>
  * Matthew Saltzman <msaltzm5@jhu.edu>
  * Buck Thompson <bthomp36@jhu.edu>
  * 600.226 Data Structures
  * Assignment P4
  *
  * Implementation of an edge class (for graphs).
  * @param <T> the type of data to store at vertices.
  */
public class WEdge<T> implements Comparable<Object> {

    /** Starting vertex of an edge. */
    private GVertex<T> source;
    /** Ending vertex of an edge. */
    private GVertex<T> end;
    /** The weight associated with the edge. */
    private double weight;
    /** True if weighted, false otherwise. */
    private boolean isWeighted;

    /** Create an unweighted edge.
     *  @param u the start
     *  @param v the end
     */
    public WEdge(GVertex<T> u, GVertex<T> v) {
        this.source = u;
        this.end = v;
        this.weight = 1;
        this.isWeighted = false;
    }

    /** Create a weighted edge.
     *  @param u the start
     *  @param v the end
     *  @param w the weight
     */
    public WEdge(GVertex<T> u, GVertex<T> v, double w) {
        this.source = u;
        this.end = v;
        this.weight = w;
        this.isWeighted = true;
    }

    /** Is a vertex incident to this edge.
     *  @param v the vertex
     *  @return true if source or end, false otherwise
     */
    public boolean isIncident(GVertex v) {
        return this.source.equals(v) || this.end.equals(v);
    }

    /** Returns the weight of the edge.
     *  @return the weight.
     */
    public double weight() {
        return this.weight;
    }

    /** Get the starting endpoint vertex.
     *  @return the vertex
     */
    public GVertex<T> source() {
        return this.source;
    }

    /** Get the ending endpoint vertex.
     *  @return the vertex
     */
    public GVertex<T> end() {
        return this.end;
    }

    /** Create a string representation of the edge.
     *  @return the string as (source,end)
     */
    public String toString() {
        return "(" + this.source + "," + this.end + "," + this.weight + ")";
    }

    /** Check if two edges are the same.
     *  @param other the edge to compare to this
     *  @return true if endpoints match, false otherwise
     */
    public boolean equals(Object other) {
        if (other instanceof WEdge) {
            WEdge<T> e = (WEdge<T>) other;
            if (this.weight() != e.weight()) {
                return false;
            }
            return this.source.equals(e.source)
                && this.end.equals(e.end)
                || this.source.equals(e.end)
                && this.end.equals(e.source);
        }
        return false;
    }

    /** Make a hashCode based on the toString.
     *  @return the hashCode
     */
    public int hashCode() {
        return this.toString().hashCode();
    }

    /**
     * Compares the edge to another edge by weight.
     * @param other the other edge.
     * @return 1 if this edge has greater weight, 0 if same, -1 if less.
     */
    public int compareTo(Object other) {
        if (other instanceof WEdge) {
            WEdge<T> e = (WEdge<T>) other;
            if (this.weight() > e.weight()) {
                return 1;
            }
            if (this.weight() == e.weight()) {
                return 0;
            }
            return -1;
        }
        return 0;
    }
}
